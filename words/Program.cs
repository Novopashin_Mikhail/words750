﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace words
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var host = Startup.CreateHostBuilder().Build();
            
            using (var serviceScope = host.Services.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
 
                try
                {
                    var myService = services.GetRequiredService<MyApplication>();
                    await myService.Run();
 
                    Console.WriteLine("Success");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}