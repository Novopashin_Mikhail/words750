using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace words
{
    public class MyApplication
    {
        private readonly ILogger _logger;
        private readonly ISeleniumService _seleniumService;
        public MyApplication(ILogger<MyApplication> logger, ISeleniumService seleniumService)
        {
            _logger = logger;
            _seleniumService = seleniumService;
        }
 
        internal async Task Run()
        {
            _logger.LogInformation("Application {applicationEvent} at {dateTime}", "Started", DateTime.UtcNow);

            await _seleniumService.Run();
 
            _logger.LogInformation("Application {applicationEvent} at {dateTime}", "Ended", DateTime.UtcNow);
 
            Console.WriteLine("PRESS <ENTER> TO EXIT");
            Console.ReadKey();
        }
    }
}