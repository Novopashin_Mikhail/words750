using System.Threading.Tasks;

namespace words
{
    public interface ISeleniumService
    {
        Task Run();
    }
}