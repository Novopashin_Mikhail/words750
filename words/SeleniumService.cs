using System;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace words
{
    public class SeleniumService : ISeleniumService
    {
        public async Task Run()
        {
            using var driver = new ChromeDriver(".");
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Navigate().GoToUrl("https://750words.com/auth");
            var webElement = driver.FindElement(By.Id("person_email_address"));
            webElement.SendKeys("novopashinwm@gmail.com");
            var webElPass = driver.FindElement(By.Id("person_password"));
            webElPass.SendKeys("28051976");
            driver.FindElement(By.Name("commit")).Click();
                
            wait.Until(x => x.FindElement(By.Id("entry_body")));

            var words = new[] {"word1", "word2", "word3"};
            
            var webElementEntryBody = driver.FindElement(By.Id("entry_body"));

            foreach (var word in words)
            {
                webElementEntryBody.SendKeys(word + Environment.NewLine);
            }
            
            wait.Until(x => x.FindElement(By.ClassName("subdued")));
            driver.FindElement(By.ClassName("subdued")).Click();

            await Task.Delay(TimeSpan.FromSeconds(30));
        }
    }
}


